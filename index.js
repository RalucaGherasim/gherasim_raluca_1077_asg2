
const FIRST_NAME = "GHERASIM";
const LAST_NAME = "RALUCA";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching() {
   var c={};
   c.pageAccessCounter=function(sectwbSite='home')
   {
       sectwbSite=new String(sectwbSite).toLowerCase();
       if(c.hasOwnProperty(sectwbSite))
       {
           c[sectwbSite]++;
       }else{
           Object.defineProperty(c,sectwbSite,
            {
                value:1,
                writable:true
            });
       }
   }
   c.getCache=function()
   {
       return this;
   }
return c;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

